#!/bin/bash

#This script runs on a provisioned aws instance for configuration purposes

sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm -y
sudo dnf install stress-ng zip git podman python3 -y

git clone https://gitlab.com/sberg1/isolation-test
