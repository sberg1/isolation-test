#!/bin/bash
# Requires two ENV vars set: TESTCMD and TESTARGS
# Executes command along with args
# Exits task and container when testrun is completed
##set -u

# Strip quotes from ENV vars
tmp1="${TESTCMD%\"}"
command="${tmp1#\"}"
tmp2="${TESTARGS%\"}"
args="${tmp2#\"}"

# Both must be defined
if [ -z "${command}" ] || [ -z "${args}" ]; then
    echo "bad syntax. ENV vars TESTCMD and TESTARGS must be passed"
    exit
else
    echo "TESTCMD = ${command}"
    echo "TESTARGS = ${args}"
fi

echo "sleep = ${SLEEP}"

# Check for bmark command
if [ ! hash "$command" &> /dev/null ]; then
    echo "$command could not be found"
    exit
fi

sleep $SLEEP

cmdString="$command $args"
echo $cmdString             # DEBUG
# Run the test with passed args
$(eval "$cmdString")
if [ $? -ne 0 ]; then
    echo "$cmdString - would not execute"
    exit
fi
echo "SUCCESS..."

# put in logic to save run results and share with Host
sleep $SLEEP

exit
