#!/bin/bash

#Configs
#Elasticsearch address
es="marquez.perf.lab.eng.rdu2.redhat.com:9200"
export es

#test duration in minutes
sca_duration=2
nsca_duration=1
#Place nsca run in the middle of the sca rn
sleep=$(( (${sca_duration}*60 - ${nsca_duration}*60)/2 ))

#Set safe application parameters
safe_cpu_perc=25
safe_mem_perc=25
#Interval between data reporting (only for latest podman version)
#TODO check podman versioning for interval?
stats_interval=1

#Preflight checks
# Must be run as root to use cgroups
if [ "$EUID" -ne 0 ]
then
    echo "Please run as root"
    exit
fi

#check for data directory, make if don't exist
if [ ! -d "./data" ]
then
    mkdir data
fi

#check core count is >= 2
cores=$(grep -c ^processor /proc/cpuinfo)
if [ $cores -lt 4 ]
then
    echo "WARNING: Recommended at least 4 CPUs or cores for most testing"
    exit
fi
if [ $cores -lt 2 ]
then
    echo "ERROR: Minimum 2 CPUs or cores for testing"
    exit
fi

#check for container build status
podman images | grep 'stress' &> /dev/null
if [ $? != 0 ]; then
    echo "Podman stress container not built. Building now.."
    podman build -t stress ./stress
fi
echo "############### PREFLIGHT CHECKS COMPLETE ###############"


#Use safe percentage to calculate cpu usage for safety critcal application
safe_cpus=$(( ($(nproc)+(100/$safe_cpu_perc)-1) / (100/$safe_cpu_perc) ))
#Account for CPU usage of memory consumption process
if [[ $safe_cpus -gt 1 ]]; then
    safe_cpus=$(( $safe_cpus - 1 ))
fi


### TEST BEGINS HERE ###
echo "############### BEGINNING FFI TEST ###############"
#Baseline Test Stage
echo "############### BASELINE STAGE ###############"
date=$(date +%Y%m%d_%H%M%S)
echo "Running Safety Critical application with $safe_cpus CPUs and $safe_mem_perc% memory usage"

#Run Safety Critical Application in container
baseline_pod_id=$(podman run -v ./data:/results:z -dt -e CPUS=$safe_cpus -e MEM=$safe_mem_perc% -e STAGE=baseline_0 -e TIME=${sca_duration}m -e SLEEP=0 -e LOG=/results/safe_bogo_ops_baseline_0_${date}.log --name=safe_baseline_0_${date} --env-file stress/runtest.env stress)

#Save start time and --interval argument to file
echo $(date +%s) $stats_interval >> ./data/safe_cpu_mem_baseline_0_${date}.dat

#Collect usage data
echo "Measuring baseline cpu and memory usage for safety critical application..."
podman stats $baseline_pod_id --format "{{.CPUPerc}} {{.MemUsageBytes}}" >> ./data/safe_cpu_mem_baseline_0_${date}.dat &

#Wait for stress container to finish
podman wait --latest --interval=1s > /dev/null 2>&1

#kill podman stats processes
killall podman

#Save end time for bogo stats
echo $(date +%s) $stats_interval >> ./data/safe_bogo_ops_baseline_0_${date}.log

echo "Baseline test complete."
echo "Cooldown..."
sleep 10
echo "Cooldown complete."
echo "############### END BASELINE STAGE ###############"
printf "\n\n"


#Concurrent Test Stage
echo "############### CONCURRENT STAGE ###############"
date=$(date +%Y%m%d_%H%M%S)
#For a shorter test on machines with a large amount of cores, use the second line
for (( i=1; i<$(nproc)+1; ++i ));
#for (( i=1; i<$(nproc)+1; i+=$(nproc)/2-1 ));
do
    echo "Running concurrent test $i..."
    #Set nonsafe memory usage percentage
    nonsafe_mem_perc=$(awk "BEGIN {print int(100*($i/$(nproc)))}")

    #Run Safety Critical Application in container
    echo "Running Safety Critical application with $safe_cpus CPUs and $safe_mem_perc% memory usage"

    #Start podman stress-ng container
    safe_pod_id=$(podman run -v ./data:/results:z -dt -e CPUS=$safe_cpus -e MEM=$safe_mem_perc% -e STAGE=concurrent_$i -e TIME=${sca_duration}m -e SLEEP=0 -e LOG=/results/safe_bogo_ops_concurrent_${i}_${date}.log --name=safe_concurrent_${i}_${date} --env-file stress/runtest.env stress)

    #Save start time and --interval arguement to file
    echo $(date +%s) $stats_interval >> ./data/safe_cpu_mem_concurrent_${i}_${date}.dat

    #Collect usage data for safe application
    echo "Measuring cpu and memory usage for safety critical application..."
    podman stats $safe_pod_id --format "{{.CPUPerc}} {{.MemUsageBytes}}" >> ./data/safe_cpu_mem_concurrent_${i}_${date}.dat &

    #Run Non Safety Critical Application in container
    echo "Running Non Safety Critical application with $i CPUs and $nonsafe_mem_perc% memory usage"
    nonsafe_pod_id=$(podman run -v ./data:/results:z -dt -e CPUS=$i -e MEM=$nonsafe_mem_perc% -e STAGE=concurrent_$i -e TIME=${nsca_duration}m -e SLEEP=${sleep} -e LOG=/results/nonsafe_bogo_ops_concurrent_${i}_${date}.log --name=nonsafe_concurrent_${i}_${date} --env-file stress/runtest.env stress)

    #Save start time and --interval arguement to file
    echo $(date +%s) $stats_interval >> ./data/nonsafe_cpu_mem_concurrent_${i}_${date}.dat

    #Collect usage data for nonsafe application
    echo "Measuring cpu and memory usage for nonsafety critical application..."
    podman stats $nonsafe_pod_id --format "{{.CPUPerc}} {{.MemUsageBytes}}" >> ./data/nonsafe_cpu_mem_concurrent_${i}_${date}.dat &

    #Wait for stress containers to finish
    podman wait --latest --interval=1s > /dev/null 2>&1

    #kill podman stats processes and plotting
    killall podman

    #Save end time for bogo stats
    echo $(date +%s) $stats_interval >> ./data/safe_bogo_ops_concurrent_${i}_${date}.log
    echo $(date +%s) $stats_interval >> ./data/nonsafe_bogo_ops_concurrent_${i}_${date}.log

    echo "Concurrent test $i complete."
    echo "Cooldown..."
    sleep 10
    echo "Cooldown complete."
done
echo "############### END CONCURRENT STAGE ###############"
printf "\n\n"



#Mitigation Techniques Test Stage
echo "############### MITIGATION TECHNIQUES STAGE ###############"
date=$(date +%Y%m%d_%H%M%S)
#For a shorter test on machines with a large amount of cores, use the second line
for (( i=1; i<$(nproc)+1; ++i ));
#for (( i=1; i<$(nproc)+1; i+=$(nproc)/2-1 ));
do

    echo "Running mitigation techniques test $i..."
    #Calculate effective utilization rates
    nonsafe_mem_perc=$(awk "BEGIN {print int(100*(($(nproc)-$i)/$(nproc)))}")
    nonsafe_cpu_perc=$(awk "BEGIN {print int(100*(($(nproc)-$i)/$(nproc)))}")
    echo "Mitigation will attempt to limit effective consumption of Non Safety Critical application to ${nonsafe_cpu_perc}% CPU and ${nonsafe_mem_perc}% memory"
    nonsafe_mem_limit=$(awk "BEGIN {print int($(grep MemTotal /proc/meminfo | awk '{print $2}') * $nonsafe_mem_perc * 0.01)}")

    #Safety critical application
    echo "Running Safety Critical application with $safe_cpus CPUs and $safe_mem_perc% memory usage"

    #Start Safety critical application in container
    safe_pod_id=$(podman run -v ./data:/results:z -dt -e CPUS=$safe_cpus -e MEM=$safe_mem_perc% -e STAGE=mitigation_$i -e TIME=${sca_duration}m -e SLEEP=0 -e LOG=/results/safe_bogo_ops_mitigation_${i}_${date}.log --name=safe_mitigation_${i}_${date} --env-file stress/runtest.env stress)

    #Save start time and --interval arguement to file
    echo $(date +%s) $stats_interval >> ./data/safe_cpu_mem_mitigation_${i}_${date}.dat

    #Collect usage data for safety critical application
    echo "Measuring cpu and memory usage for safety critical application"
    podman stats $safe_pod_id --format "{{.CPUPerc}} {{.MemUsageBytes}}" >> ./data/safe_cpu_mem_mitigation_${i}_${date}.dat &

    #Non safety critical application
    nonsafe_cpu_range=($(( $i-1 ))-$(( $(nproc) - 1 )))
    echo $nonsafe_cpu_range
    echo "Running Non Safety Critical application with $(nproc) CPUs and 100% memory usage. Using cgroups to limit CPUs to $nonsafe_cpu_range (${nonsafe_cpu_perc}%) and ${nonsafe_mem_limit}kb (${nonsafe_mem_perc}%) of memory"
    nonsafe_pod_id=$(podman run -v ./data:/results:z --cpuset-cpus=$nonsafe_cpu_range --memory=${nonsafe_mem_limit}k -dt -e CPUS=$(nproc) -e MEM=100% -e STAGE=mitigation_$i -e TIME=${nsca_duration}m -e SLEEP=${sleep} -e LOG=/results/nonsafe_bogo_ops_mitigation_${i}_${date}.log --name=nonsafe_mitigation_${i}_${date} --env-file stress/runtest.env stress)

    #Save start time and --interval arguement to file
    echo $(date +%s) $stats_interval >> ./data/nonsafe_cpu_mem_mitigation_${i}_${date}.dat

    #Collect usage data for nonsafety critical application
    echo "Measuring cpu and memory usage for nonsafety critical application"
    podman stats $nonsafe_pod_id --format "{{.CPUPerc}} {{.MemUsageBytes}}" >> ./data/nonsafe_cpu_mem_mitigation_${i}_${date}.dat &

    #Wait for stress containers to finish
    podman wait --latest --interval=1s > /dev/null 2>&1

    #kill podman stats processes
    killall podman

    #Save end time for bogo stats
    echo $(date +%s) $stats_interval >> ./data/safe_bogo_ops_mitigation_${i}_${date}.log
    echo $(date +%s) $stats_interval >> ./data/nonsafe_bogo_ops_mitigation_${i}_${date}.log

    echo "Cooldown..."
    sleep 10
    echo "Cooldown complete."
done
echo "############### END MITIGATION TECHNIQUES STAGE ###############"
printf "\n\n"

echo "All stages complete."

#Process and package data
echo "Processing and packaging data.."
mkdir ./data/archives
cd ./data/archives
for f in ../*; do python3 ../../formatES.py $f; done
cd ../..
zip -o archives.zip ./data/archives/*
zip -o data.zip ./data/*
rm -rf ./data/*
echo "Processing complete. data.zip and archives.zip can be removed."

#Clean up
podman rm -f $(podman ps -a -f "status=exited" -q) >> /dev/null
