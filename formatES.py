from datetime import datetime, timedelta
import os
import uuid
import hashlib
import json

es_index = {"_index": "snafu-ffi-summary", "_op_type": "create", "_source": {}, "_id": "", "run_id": "NA"}


def get_bytes(size, suffix):
    size = float(size)
    suffix = suffix.lower()

    if suffix == 'kb' or suffix == 'kib':
        return size * 1024
    elif suffix == 'mb' or suffix == 'mib':
        return size * 1024**2
    elif suffix == 'gb' or suffix == 'gib':
        return size * 1024**3


def parse(filepath, result_summary):

    #Name of test for outfile path and ES purposes
    test = os.path.basename(filepath).split('.')[0]

    #Grab safe/nonsafe, stage, and iteration from test
    app_type = test.split('_')[0]
    metric = test.split('_')[1]
    stage = test.split('_')[3]
    iteration = test.split('_')[4]
    if iteration.isalpha():
        iteration = 0

    #Specify app, stage, iteration, and metric for ES
    result_summary["test_config"] = {"app": app_type, "stage": stage, "iteration": iteration, "metric": ""}

    #Bring data into memory
    with open(filepath) as f:
        #First line is a timestamp so exclude
        lines = f.readlines()


    #Two types of data files, bogo and CPU/Memory files.
    if metric != 'bogo':
        #CPU/Memory data file
        outfile = test + '_CPU_MEMORY.archive'

        #Parse timestamp
        timestamp = lines[0].split()[0]
        interval = timedelta(seconds=int(lines[0].split()[1]))
        sample_starttime = datetime.fromtimestamp(int(timestamp))

        #Parse data and write to archive file
        with open(outfile, 'w') as f:

            #For each line in data file
            #Skip timestamp and first data point
            for line in lines[2:-2]:

                #Create ES result index with CPU/Memory usage as test result and datetime as time stamp
                result_summary["date"] = sample_starttime.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

                #Parse CPU first then memory from same line. Will create two archive objects.
                for i in range(2):
                    if i == 0:
                        #Parse CPU usage
                        result_summary["test_config"]["metric"] = "cpu"
                        cpu_usage = line.split()[0].strip('%')
                        if (cpu_usage == "--"):
                            cpu_usage = "0"
                        cpu_usage = float(cpu_usage)/100

                        result_summary["test_results"] = {"CPU_usage": {"cores": cpu_usage}}
                    else:
                        #Parse Memory usage
                        result_summary["test_config"]["metric"] = "memory"
                        size = line.split()[1][:-3]
                        suffix = line.split()[1][-3:]
                        mem_usage = get_bytes(size, suffix)
                        result_summary["test_results"] = {"Memory_usage": {"bytes": mem_usage}}

                    #Format ES output
                    output = es_index
                    output["_source"] = result_summary
                    output["_id"] = hashlib.sha256(str(result_summary).encode()).hexdigest()

                    #write ES result to .archive file
                    f.write(json.dumps(output)+'\n')

                #increment datetime by podman stats --interval arg used to generate results
                sample_starttime = sample_starttime + interval

    else:
        #Bogo data file
        outfile = test + '_BOGO.archive'

        #Parse timestamp
        timestamp = lines[-1].split()[0]
        sample_starttime = datetime.fromtimestamp(int(timestamp))
        result_summary["date"] = sample_starttime.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

        with open(outfile, 'w') as f:
            for line in lines[4:6]:
                if "cpu" in line:
                    #CPU line
                    result_summary["test_config"]["metric"] = "cpu"
                else:
                    #Memory line
                    result_summary["test_config"]["metric"] = "memory"

                bogo_ops = float(line.split()[4])
                bogo_ops_s = float(line.split()[-2])

                #Format ES output
                result_summary["test_results"] = {"Throughput": {"bogo ops": bogo_ops, "bogo ops/s": bogo_ops_s}}
                output = es_index
                output["_source"] = result_summary
                output["_id"] = hashlib.sha256(str(result_summary).encode()).hexdigest()

                #write ES result to Bogo .archive file
                f.write(json.dumps(output)+'\n')


def main(argv):
    #Format ES result_summary json
    result_uuid = os.getenv("uuid", str(uuid.uuid4()))
    result_summary = {
        "uuid": result_uuid,
        "user": "sberg",
        "clustername": "1-aws_a1.xlarge-baremetal-centos8",
        "date": 0,
        "sample": 1,
        "test_config": {},
        "test_results": {},
        "run_id": "NA"
    }

    #Need input file arg
    if len(argv) != 2:
        raise SystemExit(f'Usage: {argv[0]} ' 'formatES <input file>')

    #take arg as filepath
    filepath = argv[1]

    parse(filepath, result_summary)

if __name__ == '__main__':
    import sys
    main(sys.argv)
