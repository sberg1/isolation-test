#!/bin/bash

#This script will provision an aws instance and ssh in once the instance is up

#TODO use args?
#Take instance type as variable

#### Change these as necessary #####
instance_type="a1.xlarge"
region="us-east-2"
#ARM centos 8
image_id="ami-035734c938e7da7af"
aws_key_name="<AWS_KEY_NAME_HERE>"
aws_security_group="<AWS_SG_ID_HERE>"
ssh_key_file="<PATH_TO_KEY_FILE_HERE>"
#### ----------------------------- ####

#Create new instance with ssh keys and port 22 open (security group sberg-dev) if needed
#else start old one
#parse instance id
instance_id=$(aws ec2 run-instances --image-id $image_id --count 1 --instance-type $instance_type --key-name $aws_key_name --security-group-ids $aws_security_group | jq '.Instances[0].InstanceId')
instance_id="${instance_id%\"}"
instance_id="${instance_id#\"}"
echo $instance_id


#monitor status of instance
instance_status=$(aws ec2 describe-instances --instance-ids $instance_id | jq '.Reservations[0].Instances[0].NetworkInterfaces[0].Attachment.Status')
instance_status="${instance_status%\"}"
instance_status="${instance_status#\"}"
echo $instance_status


#wait for instance to come online
while [ $instance_status != "attached" ]; do
    instance_status=$(aws ec2 describe-instances --instance-ids $instance_id | jq '.Reservations[0].Instances[0].NetworkInterfaces[0].Attachment.Status')
    instance_status="${instance_status%\"}"
    instance_status="${instance_status#\"}"
    echo $instance_status
    sleep 2
done

#wait for instance to be ready for ssh
instance_ready=$(aws ec2 describe-instance-status --instance-ids $instance_id | jq '.InstanceStatuses[0].InstanceStatus.Status')
instance_ready="${instance_ready%\"}"
instance_ready="${instance_ready#\"}"
while [ $instance_ready != "ok" ]; do
    instance_ready=$(aws ec2 describe-instance-status --instance-ids $instance_id | jq '.InstanceStatuses[0].InstanceStatus.Status')
    instance_ready="${instance_ready%\"}"
    instance_ready="${instance_ready#\"}"
    echo $instance_ready
    sleep 5
done

#once up, parse ip address
instance_ip=$(aws ec2 describe-instances --instance-ids $instance_id | jq '.Reservations[0].Instances[0].NetworkInterfaces[0].Association.PublicDnsName')
instance_ip="${instance_ip%\"}"
instance_ip="${instance_ip#\"}"

echo $instance_ip

#Transfer scripts and conf (uncomment for sysbench setup)
scp -i $HOME/Downloads/aws-sberg.pem -o StrictHostKeyChecking=no ./aws_bootup.sh centos@$instance_ip:~

#Set up instance env (uncomment for sysbench setup)
ssh -i $HOME/Downloads/aws-sberg.pem -o StrictHostKeyChecking=no centos@$instance_ip '~/aws_bootup.sh'

#Log in to instance
ssh -i $HOME/Downloads/aws-sberg.pem -o StrictHostKeyChecking=no centos@$instance_ip

#Delete or stop instance
# (uncomment to terminate instance when finished)
#aws ec2 terminate-instances --instance-ids $instance_id
