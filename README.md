# Isolation-test

Repo for files related to running isolation testing

**Important to build podman container as root**

You can use [aws_provision.sh](aws_provision.sh) to provision an AWS instance of a type of your choosing with [aws_bootup.sh](aws_bootup.sh) already included.

[aws_bootup.sh](aws_bootup.sh) will setup an instance or machine to begin isolation testing

[run.sh](run.sh) will run all stages of the isolation test with default parameters (iteration steps will be based on number of CPU cores on target machine)
After running, generated plots will be stored as .png files in the /plots directory and raw data files will be stored as .dat files in the /plots/data directory.

[formatES.py](formatES.py) will format raw .dat files which include CPU and Memory results (located in plots/data with "cpu_mem" in file name) into an ElasticSearch ingestible format. See [example_CPU.archive](example_CPU.archive) and [example_MEMORY.archive](example_MEMORY.archive) for examples of ElasticSearch archive file formats
